
function calcCircleGeometries(radius){
 const pi=Math.PI;
 var area=pi*radius*radius;
 var circumference=2*pi*radius;
 var diameter = 2 * radius;
 var geometries=[radius, diameter, circumference, area];
 return geometries;
}

function calcRow(cl){
    var circle1=calcCircleGeometries(Math.random()*5);
    var row = document.getElementsByClassName(cl);
    row[0].innerHTML = circle1[0].toFixed(2);
    row[1].innerHTML=circle1[1].toFixed(2);
    row[2].innerHTML = circle1[2].toFixed(2);
    row[3].innerHTML=circle1[3].toFixed(2);
}

calcRow("row1");
calcRow("row2");
calcRow("row3");