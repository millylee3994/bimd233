    function calcRow(cl, weather){
        console.log(weather.description);
    var row = document.getElementsByClassName(cl);
    row[0].innerHTML = weather.day+"</br>"+ weather.date;
    row[1].innerHTML = weather.high+"<span>&#176;</span>"+"/"+"<sub>"+ weather.low +"<span>&#176;</span>"+"</sub>";
    row[2].innerHTML = "<img src='" + weather.description + ".svg'>" + "&nbsp;&nbsp" + weather.description;
    row[3].innerHTML ="<img src='droplet.svg'>"+ "&nbsp;&nbsp" + weather.precip;
    row[4].innerHTML = weather.wind;
    row[5].innerHTML = weather.humidity;
}

//就是物件陣列
//wx_data = []  <== js 陣列宣告
//wx_data = [1,2,3]  <== 陣列存放三個元素  wx_data[0] = 1   wx_data[1] = 2 ......
//下列範例只是將 wx_data = [1,2,3] 的 1 2 3 "元素" 改成存 "物件"  進去
var wx_data = [ 
    {    day: "FRI",    high: 82,    low: 55,    description:"Sunny",      precip:"0%",     wind:"N 9 mph",     humidity:"46%",  date:"May 13", }, 
    {    day: "SAT",    high: 75,    low: 52,    description:"Cloudy",      precip:"20%",    wind:"WSW 6 mph",   humidity:"54%",  date:"May 14", },
    {    day: "SUN",    high: 69,    low: 52,    description:"Showers",     precip:"60%",    wind:"SSW 8 mph",   humidity:"70%",  date:"May 15", },
    {    day: "MON",    high: 69,    low: 48,    description:"Cloudy",      precip:"20%",    wind:"SSW 8 mph",   humidity:"62%",  date:"May 16", }, 
    {    day: "TUE",    high: 68,    low: 51,    description:"Showers",     precip:"40%",    wind:"SW 7 mph",    humidity:"57%",  date:"May 17", }
    ];


for(let i=0; i < wx_data.length; i++){
    calcRow("row"+(i+1) ,wx_data[i]);
}

function disp_prompt()
{
    var state="idle"
    do{
        var cmd = prompt('Enter a command:' + state);
        if(cmd == "next"){
            switch(state){
                case "idle":
                    state = "s1"; // if state was idle, set state to s1
                    break;   
                case "s1":
                    state = "s2"; // if state was s2, set state to s3
                    break;
                case "s2":
                    state = "s3"; 
                    break;
                case "s3":
                    state = "s4"; 
                    break;
                case "s4":
                    state = "s5";
                    break;
                case "s5":
                    cmd="quit"; // if state was s5, quit the loop
                    break;
            }
        }
    }while(cmd != "exit" && cmd != "quit")
}
//= set a var, ==check a var

