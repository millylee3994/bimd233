function flight(airline, num, origin, dest, dep, arival, gate){
    var flight = {};
    flight.airline = airline;
    flight.number = num;
    flight.origin = origin;
    flight.destination = dest;
    flight.dep_time = dep;
    flight.arrival_time = arival;
    flight.arrival_gate = gate;
    flight.getDuration = function() {
        var t2 = new Date(dep_time);
        var t3 = new Date(arrival_time);
        var diff = t3.getTime() - t2.getTime();  //ms
        return diff; // difference in time
    }
    return flight;
}

function calcRow(cl, flight){
    var row = document.getElementsByClassName(cl);
    row[0].innerHTML = flight.airline;
    row[1].innerHTML= flight.number;
    row[2].innerHTML = flight.origin;
    row[3].innerHTML = flight.destination;
    row[4].innerHTML= flight.dep_time;
    row[5].innerHTML = flight.arrival_time;
    row[6].innerHTML= flight.arrival_gate;
}

calcRow("row1",flight("Alaska Airlines", "B500", "John Wayne (KSNA)", "San Francisco Intl (KSFO)", "Wed 05:43PM EST", "Wed 08:15PM PST", "G21"));
calcRow("row2",flight("Alaska Airlines", "B520", "Seattle-Tacoma Intl (KSEA)", "Portland Int", "Wed 01:47PM PST", "Wed 01:47PM PST", "G1"));
calcRow("row3",flight("Alaska Airlines", "B566", "San Francisco Intl (KSFO)", "	John Wayne (KSNA)", "Wed 06:54PM EST", "Wed 09:44PM PST", "G5"));
calcRow("row4",flight("Alaska Airlines", "B020", "Portland Int", "Los Angeles Intl (KLAX)", "Wed 07:28PM EST", "Wed 10:07PM PST", "G6"));
calcRow("row5",flight("Alaska Airlines", "B877", "Seattle-Tacoma Intl (KSEA)", "Los Angeles Intl (KLAX)", "Wed 05:55PM EST", "Wed 07:53PM PST", "G17"));